#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import logging
import json
import traceback
from datetime import datetime
from OpenhabApi import Openhab

dir_path = os.path.dirname(os.path.realpath(__file__))

logging.basicConfig(filename='{dir}/ErrorController.log'.format(dir=dir_path), filemode='w', format='%(asctime)s - %(message)s',
                    level=logging.INFO)


def read_json_file(file_path):

	data = ''
	if os.path.exists(file_path):
		with open(file_path) as f:
			try:
				data = json.load(f)
			except:
				data
	return data


def check_excluded_days(things):

	excluded_days_for_switch = read_json_file(dir_path + "ExcludedDates.json")

	today = datetime.now().strftime("%d-%m-%Y")

	for thing in things:
		if 'ExcludedDates' not in excluded_days_for_switch:
			openhab.change_item_state(thing, 'ON')
			return "Changing item state " + thing
		elif thing in excluded_days_for_switch['ExcludedDates']:
			if today in excluded_days_for_switch['ExcludedDates'][thing]:
				return "This day is in Excluded Days"
		openhab.change_item_state(thing, 'ON')
		return "This Day is not excluded, Changing item state " + thing
	return "No things found"


if __name__ == '__main__':
	try:
		openhab = Openhab()

		things = [item for item in sys.argv[1:]]
		body = json.dumps({'devices': things})
		result = check_excluded_days(things)
		print(result)
	except Exception:
		logging.info('{}'.format(traceback.format_exc()))

