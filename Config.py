OpenhabApi = {
	"URL": "http://localhost",
	"PORT": 8080
}

OpenhabConfig = {
	"CONFIG_ITEM_NAME": 'OpenhabConfig',
	"ITEM_FILE_NAME": 'ConfigItem.items',
	"CONFIG_RULE_FILE_NAME": "OpenhabConfig.rules",
	"OPENHAB_PATH": "/etc/openhab2"
}

Mqtt = {
	"CONFIG_BROKER_CHANNEL": '/hub/config',
	"CONFIG_BROKER_CHANNEL_CONFIRM": '/hub/config/confirm',
	"MQTT_CFG_PATH": '/etc/openhab2/services/mqtt.cfg'
}

Days = {
	0: "SUN",
	1: "MON",
	2: "TUE",
	3: "WED",
	4: "THU",
	5: "FRI",
	6: "SAT"
}

DeviceType = {
	1: 'switch_binary',
	2: 'sensor_temperature',
	3: 'electric_meter',
	4: 'switch_binary',
	5: 'switch_binary',
	8: 'switch_binary'
}

DIFFERENT_IN_MIN = 2
