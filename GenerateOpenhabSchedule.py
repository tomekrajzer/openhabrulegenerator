#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import traceback
import logging
import os

from OpenhabApi import Openhab
from OpenhabGenerator import OpenhabRuleGenerator
from datetime import datetime, timedelta
from Config import DIFFERENT_IN_MIN, DeviceType

dir_path = os.path.dirname(os.path.realpath(__file__))

logging.basicConfig(filename='{dir}/Error.log'.format(dir=dir_path), filemode='w', format='%(asctime)s - %(message)s',
                    level=logging.INFO)


def get_dict_by_day(days, day_number):

    for day in days:
        if day == day_number:
            return days[day]


def check_if_should_implement_off(days):

    for day in days:
        days[day] = sorted(days[day], key=lambda k: k['onFromHour'] * 60 + k['onFromMin'])

    for day in days:
        for enum, config in enumerate(days[day]):
            time_to = datetime.now().replace(hour=config['onToHour'], minute=config['onToMin'])
            # check if this is last config, when last compare with first config from next day, if not compare with next
            # config of this day
            if config == days[day][-1]:
                if day == 6:
                    next_day_dict = get_dict_by_day(days, 0)
                else:
                    next_day_dict = get_dict_by_day(days, day + 1)
                if next_day_dict:
                    time_from_next = datetime.now().replace(hour=next_day_dict[0]['onFromHour'], minute=next_day_dict[0]['onFromMin']) + timedelta(days=1)
                    time_diff = time_from_next - time_to
                    minutes, seconds = divmod(time_diff.total_seconds(), 60)
                    # if time differences is lower or equal 2 minutes, don't implement turn off rule this day
                    if minutes <= DIFFERENT_IN_MIN:
                        days[day][enum]['implement_off'] = False
                    else:
                        days[day][enum]['implement_off'] = True
                else:
                    days[day][enum]['implement_off'] = True
            else:
                config_next = days[day][enum + 1]
                time_from_next = datetime.now().replace(hour=config_next['onFromHour'], minute=config_next['onFromMin'])
                time_diff = time_from_next - time_to
                minutes, seconds = divmod(time_diff.total_seconds(), 60)
                # if time differences is lower or equal 2 minutes, don't implement turn off rule this day
                if minutes <= DIFFERENT_IN_MIN:
                    days[day][enum]['implement_off'] = False
                else:
                    days[day][enum]['implement_off'] = True
    return days


def get_sensor_names(space):
    switches = []
    temperature_sensor_name = ''
    opposite_rule = False
    rs_air_conditioners = list(filter(lambda device: device['deviceType'] == 8, space['devices']))

    if len(rs_air_conditioners) > 0:
        switches = [device['deviceName'] + '_switch_binary' for device in rs_air_conditioners]
        temperature_sensor_name = next((e['deviceName'] + '_sensor_temperature' for e in space['devices'] if e['deviceType'] == 2), '')
        opposite_rule = True
    else:
        for device in space['devices']:
            if 'switch_binary' == DeviceType[device['deviceType']]:
                switches.append(device['deviceName'] + '_switch_binary')

            elif 'sensor_temperature' == DeviceType[device['deviceType']]:
                temperature_sensor_name = device['deviceName'] + '_sensor_temperature'
    return switches, temperature_sensor_name, opposite_rule


def main():
    try:
        openhab = Openhab()
        openhab_rules = OpenhabRuleGenerator()
        if not openhab.this_is_my_config:
            print("This config is not for me")
            return 0
        if len(openhab.spaces) == 0:
            print("No spaces found")
            return 0

        for space in openhab.spaces:
            switches, temperature_sensor_name, opposite_rule = get_sensor_names(space)

            if len(switches) == 0:
                continue

            grouped_days = {day['day']: [day] for day in openhab.config['dayConfigs']}
            if temperature_sensor_name != '':
                openhab_rules.generate_rule_file_with_sensor_name(temperature_sensor_name, switches, grouped_days, space['spaceName'], opposite_rule)
            else:
                openhab_rules.generate_rule_file(switches, grouped_days, space['spaceName'], opposite_rule)

            if 'excludedDates' in openhab.config:
                working_profile = 'ExcludedDates'
                openhab_rules.generate_excluded_days_file(openhab.config['excludedDates'], switches, working_profile)

        print("OK")
    except Exception:
        logging.info('{}'.format(traceback.format_exc()))
        print("ERROR")


if __name__ == '__main__':
    main()
