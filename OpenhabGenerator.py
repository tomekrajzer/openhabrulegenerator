import string
import json
import os
import sys
from Config import Days, OpenhabConfig
from datetime import datetime, timedelta
from FileContents import functionRuleFileContent, functionRuleOppositeFileContent, executionCommand, offCommand, \
	ruleCron, ruleCase, ruleCaseCondition, ruleItemChanged


class OpenhabRuleGenerator:

	def __init__(self):
		self.openhab_path = OpenhabConfig['OPENHAB_PATH']

	def generate_rule_on(self, hour, minute, day_name, devices, space_name):
		rule_cron_on = ruleCron[:]
		rule_cron_on[0] = ruleCron[0].format(day=day_name, space=space_name)
		rule_cron_on[2] = ruleCron[2].format(day=day_name, minutes=minute, hours=hour)

		for device in devices:
			rule_cron_on.insert(4, executionCommand.format(device=device, openhab=self.openhab_path))
		return rule_cron_on

	@staticmethod
	def generate_rule_off(hour, minute, day_name, devices, space_name):

		rule_cron_off = ruleCron[:]
		rule_cron_off[0] = rule_cron_off[0].format(day=day_name, space=space_name)
		rule_cron_off[2] = rule_cron_off[2].format(day=day_name, minutes=minute, hours=hour)
		for device in devices:
			rule_cron_off.insert(4, offCommand.format(device=device))
		return rule_cron_off

	@staticmethod
	def get_dict_by_day(days, day_number):

		for day in days:
			if day == day_number:
				return days[day]

	def check_if_should_implement_off(self, days):

		for day in days:
			for enum, config in enumerate(days[day]):
				time_to = datetime.now().replace(hour=config['onToHour'], minute=config['onToMin'])
				# check if this is last config, when last compare with first config from next day, if not compare with next
				# config of this day
				if day == 6:
					next_day_dict = self.get_dict_by_day(days, 0)
				else:
					next_day_dict = self.get_dict_by_day(days, day + 1)
				if next_day_dict:
					time_from_next = datetime.now().replace(hour=next_day_dict[0]['onFromHour'],
					                                        minute=next_day_dict[0]['onFromMin']) + timedelta(days=1)
					time_diff = time_from_next - time_to
					minutes, seconds = divmod(time_diff.total_seconds(), 60)
					# if time differences is lower or equal 2 minutes, don't implement turn off rule this day
					if minutes <= 2:
						days[day][enum]['implement_off'] = False
					else:
						days[day][enum]['implement_off'] = True
				else:
					days[day][enum]['implement_off'] = True
		return days

	def generate_rule_file(self, devices, days, space_name, opposite_rule):

		content_rule = ""
		days_not_scheduled = Days
		days = self.check_if_should_implement_off(days)
		for item in days:
			for day in days[item]:
				onFromMin = day['onToMin'] if opposite_rule else day['onFromMin']
				onFromHour = day['onToHour'] if opposite_rule else day['onFromHour']
				onToMin = day['onFromMin'] if opposite_rule else day['onToMin']
				onToHour = day['onFromHour'] if opposite_rule else day['onToHour']

				rule_off = self.generate_rule_off(onToHour, onToMin, Days[day['day']], devices, space_name)
				rule_on = self.generate_rule_on(onFromHour, onFromMin, Days[day['day']], devices, space_name)

				if not day[
					'implement_off'] and opposite_rule:  # if device should be on to 00:00 we not implements off rule
					rule_off = self.generate_rule_off(onToHour, onToMin, Days[day['day']], devices, space_name)
					rule_on = ''
				elif not day['implement_off'] and not opposite_rule:
					rule_on = self.generate_rule_on(onFromHour, onFromMin, Days[day['day']], devices, space_name)
					rule_off = ''

				content_rule += rule_on + rule_off
				del (days_not_scheduled[day['day']])
		# turn off devices at midnight if day is excluded from schedule
		for day in days_not_scheduled:
			if opposite_rule:
				rule_on = self.generate_rule_on(0, 0, days_not_scheduled[day], devices, space_name)
				content_rule += rule_on
			else:
				rule_off = self.generate_rule_off(0, 0, days_not_scheduled[day], devices, space_name)
				content_rule += rule_off
		self.save_file(space_name, content_rule, self.openhab_path + '/rules/', "rules")

	def generate_rule_file_with_sensor_name(self, temperature_sensor_name, devices, days, space_name, opposite_rule):

		func = functionRuleFileContent
		if opposite_rule:
			func = functionRuleOppositeFileContent
			func[9] = func[9].format(device=devices[0], openhab=self.openhab_path)
			func[4] = func[4].format(device=devices[0])
		else:
			func[4] = func[4].format(device=devices[0])
			func[6] = func[6].format(device=devices[0], openhab=self.openhab_path)
		content_rule = func

		rule_switch_case = ruleItemChanged
		rule_switch_case[0] = rule_switch_case[0].format(space_name=space_name)
		rule_switch_case[2] = rule_switch_case[2].format(sensor_name=temperature_sensor_name)
		for day in days:
			case_rule = ruleCase[:]
			case_rule[0] = ruleCase[0].format(day=day if day != 0 else 7)
			conditions = []
			for config in days[day]:

				if config['temperatureMin'] and config['temperatureMax']:
					condition = ruleCaseCondition[:]
					condition[0] = ruleCaseCondition[0].format(HourOn=config['onFromHour'], MinOn=config['onFromMin'], HourOff=config['onToHour'], MinOff=config['onToMin'],)
					condition[1] = ruleCaseCondition[1].format(sensor_name=temperature_sensor_name, tmpMin=config['temperatureMin'], tmpMax=config['temperatureMax'])
					conditions += condition
			case_rule[1:1] = conditions
			rule_switch_case[5:5] = case_rule

		content_rule += rule_switch_case
		self.save_file(space_name, content_rule, self.openhab_path + '/rules/', "rules")

	def generate_excluded_days_file(self, excluded_days, switches, working_profile_name):

		excluded_days_for_switch = self._read_json_file(
			"{openhab}/scripts/OpenhabRuleGenerator/{name}.json".format(name=working_profile_name,
			                                                            openhab=self.openhab_path))

		if not excluded_days_for_switch:
			excluded_days_for_switch = {'ExcludedDates': {}}

		for switch in switches:
			excluded_days_for_switch["ExcludedDates"][switch] = excluded_days
		self.save_file(working_profile_name, json.dumps(excluded_days_for_switch), "{}/scripts/OpenhabRuleGenerator/".format(self.openhab_path), "json")

	def save_file(self, file_name, content, path, file_type):
		original = sys.stdout
		with open(
				"{file_path}/{space_name}.{type}".format(
					space_name=self._format_filename(file_name), file_path=path, type=file_type),
				"w") as file_to_write:
			if not content:
				file_to_write.truncate()
			else:
				sys.stdout = file_to_write
				for line in content:
					print(line)
				sys.stdout = original

	@staticmethod
	def _read_json_file(file_path):
		data = ''
		if os.path.exists(file_path):
			with open(file_path) as f:
				try:
					data = json.load(f)
				except:
					data
		return data

	@staticmethod
	def _format_filename(name):

		valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
		filename = ''.join(c for c in name if c in valid_chars)
		filename = filename.replace(' ', '')
		return filename
