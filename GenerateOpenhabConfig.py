#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import re
import os

from Config import Mqtt, OpenhabConfig

dir_path = os.path.dirname(os.path.realpath(__file__))


def read_broker_name_from_file():

    with open(Mqtt['MQTT_CFG_PATH'], 'r') as f:
        read_data = f.read()
        broker_name_re = re.search('.+?(?=.url)', read_data)
        broker_name = broker_name_re.group(0)
        f.closed
    return broker_name


def generate_config_item_file(item_name, broker_name):

    content = "String {item_name} \"{item_name}\" {{ mqtt=\"<[{broker_name}:{broker_channel}:state:default]\" }}".format(
        item_name=item_name, broker_name=broker_name, broker_channel=Mqtt['CONFIG_BROKER_CHANNEL'])
    with open("/etc/openhab2/items/{item_file}".format(item_file=OpenhabConfig['ITEM_FILE_NAME']), "w") as file_to_write:
        file_to_write.write(content)


def generate_config_item_rule(item_name, broker_name):

    content = """
rule "Openhab Config"
when
    Item {item_name} changed
then
    val String script_output = executeCommandLine("/usr/bin/python3 {path}/GenerateOpenhabSchedule.py", 7000)
    publish("{broker_name}","{confirm_channel}", script_output)
    logInfo("OpenhabScenesOUTPUT", script_output)
end
    """.format(item_name=item_name, broker_name=broker_name, confirm_channel=Mqtt['CONFIG_BROKER_CHANNEL_CONFIRM'], path=dir_path)
    with open("/etc/openhab2/rules/{item_file}".format(item_file=OpenhabConfig['CONFIG_RULE_FILE_NAME']), "w") as file_to_write:
        file_to_write.write(content)


def main():
    broker_name = read_broker_name_from_file()
    generate_config_item_file(OpenhabConfig['CONFIG_ITEM_NAME'], broker_name)
    generate_config_item_rule(OpenhabConfig['CONFIG_ITEM_NAME'], broker_name)


if __name__ == '__main__':
    main()
