import requests
from Config import OpenhabConfig, OpenhabApi


class Openhab:

	def __init__(self):
		self.url = OpenhabApi['URL']
		self.port = str(OpenhabApi['PORT'])
		self.config = self.get_scenes()
		self.switches = [switch['name'].split('_alarm_power')[0].split('_switch_binary')[0].split('_sensor_binary')[0].split('_alarm_general')[0]
		                 for switch in self.get_all_switches()]
		self.this_is_my_config = self.check_if_config_for_me(self.config['spaces'])
		self.spaces = self.get_mine_spaces_from_config(self.config['spaces'])

	@staticmethod
	def _check_request_code(request):

		if request.status_code != 200:
			request.raise_for_status()
		return None

	def _post(self, endpoint, body):

		response = requests.post(self.url + ':' + self.port + '/rest/' + endpoint, data=body)
		self._check_request_code(response)

	def _get(self, endpoint):

		response = requests.get(self.url + ':' + self.port + '/rest/' + endpoint, timeout=10)
		self._check_request_code(response)
		return response.json()

	def change_item_state(self, item_name, state):

		self._post('items/' + item_name, state)

	def get_scenes(self):

		response = self._get('items/' + OpenhabConfig['CONFIG_ITEM_NAME'] + '/state')
		return response

	def get_mine_spaces_from_config(self, spaces):

		mine_spaces = []
		for space in spaces:
			for device in space['devices']:
				if device['deviceName'] in self.switches:
					mine_spaces.append(space)
					break
		return mine_spaces

	def get_all_things(self):

		response = self._get('things')
		return response

	def get_all_switches(self):
		response = self._get('items?type=Switch')
		return response

	def get_items(self, fields=None):

		if fields:
			url = '?fields='
			for field in fields:
				url += field
				if field != fields[-1]:
					url += '%2C'
		else:
			url = ''

		response = self._get('items' + url)
		return response

	def get_item_state(self, openhab_item):

		response = self._get('items/' + openhab_item + '/state')
		return response

	def change_switches_state(self, switches):

		switches_to_change = {}
		items_state = self.get_items(['state', 'name'])
		for item in items_state:
			for switch in switches:
				if item['name'] == switch and item['state'] != switches[switch]:
					switches_to_change[switch] = switches[switch]
		for item in switches_to_change:
			self.change_item_state(item, switches_to_change[item])

	def check_if_config_for_me(self, spaces):

		items_from_config = [device['deviceName'] for space in spaces for device in space['devices']]

		if len(set(self.switches) & set(items_from_config)) > 0:
			return True
		else:
			return False
