executionCommand = "executeCommandLine(\"/usr/bin/python3 {openhab}/scripts/OpenhabRuleGenerator/DeviceController.py {device}\" , 7000)"
offCommand = "\t\t{device}.sendCommand(OFF)\n"

functionRuleOppositeFileContent = [
	"val Functions$Function3<GenericItem, Number, Number, Void> ChangeItemState = [",
	"\tGenericItem CurTemp,",
	"\tNumber minTemp,",
	"\tNumber maxTemp |\n",
	"\tval Plug1 = {device}\n",
	"\tif (CurTemp.state <= minTemp) {",
	"\t\t" + executionCommand,
	"\t}",
	"\telse if (CurTemp.state >= maxTemp) {",
	"\t\tPlug1.sendCommand(OFF)",
	"\t}",
	"]"
]

functionRuleFileContent = [
	"val Functions$Function3<GenericItem, Number, Number, Void> ChangeItemState = [",
	"\tGenericItem CurTemp,",
	"\tNumber minTemp,",
	"\tNumber maxTemp |\n",
	"\tval Plug1 = {device}\n",
	"\tif (CurTemp.state <= minTemp) {",
	"\t\tPlug1.sendCommand(OFF)",
	"\t}",
	"\telse if (CurTemp.state >= maxTemp) {",
	"\t\t" + executionCommand,
	"\t}",
	"]"
]

ruleCron = [
	"rule \"turn {space} {day} on\"",
	"when",
	"\tTime cron \"0 {minutes} {hours} ? * {day} *\"",
	"then",
	"end\n"
]

ruleItemChanged = [
	"rule \"{space_name} turn heaters on\"",
	"when",
	"\tItem {sensor_name} changed",
	"then",
	"\tswitch now.getDayOfWeek {",
	"}\n"
	"end\n"

]

ruleCase = [
	"\tcase {day}: {{",
	"\t}"
]

ruleCaseCondition = [
	"\t\tif (now.getMinuteOfDay >= {HourOn}*60+{MinOn} && now.getMinuteOfDay < {HourOff}*60+{MinOff}) {{",
	"\t\t\tChangeItemState.apply({sensor_name}, {tmpMin}, {tmpMax})",
	"\t\t}",
]
